[fidel@master ~]$ cat logrotate_install.yml
---
- src: /home/fidel/logrotate_role.tar.gz
  name: logrotate_role
...

logrotate.yml
---
# Manage logrotate configs
- hosts: all
  name: logrotate
  become: "{{ use_sudo|default(True) }}"
  gather_facts: False
  roles:
    - logrotate


#!/bin/bash

HOME=/home/srv_ansible/practicando_a_fondo

mkdir -p $HOME/logrotate_role/{defaults,files,tasks,vars,meta}
touch $HOME/logrotate_role/defaults/main.yml
touch $HOME/logrotate_role/vars/main.yml
touch $HOME/logrotate_role/meta/main.yml

cat > $HOME/logrotate_role/tasks/main.yml << EOF
## tasks/main.yml
---
- name: Install logrotate
  yum:
    name: logrotate
    state: installed

- name: Ensure /etc/logrotate.d
  file:
    path: /etc/logrotate.d
    state: directory
    owner: root
    group: root
    mode: 0755

- name: Deploy /etc/logrotate.conf
  copy:
    src: logrotate.conf
    dest: /etc/logrotate.conf
    owner: root
    group: root
    mode: 0644

- name: Deploy /etc/cron.daily/logrotate
  copy:
    src: logrotate.sh
    dest: /etc/cron.daily/logrotate
    owner: root
    group: root
    mode: 0755
  
- name: Enforce /etc/logrotate.d/syslog
  copy:
    src: syslog
    dest: /etc/logrotate.d/syslog
    owner: root
    group: root
    mode: 0644

EOF


cat > $HOME/logrotate_role/files/logrotate.conf << EOF
## files/logrotate.conf
# see "man logrotate" for details
# rotate log files daily
daily

# keep 2 days worth of logs
rotate 2

# create new (empty) log files after rotating old ones
create

# use date as a suffix of the rotated file
dateext

# uncomment this if you want your log files compressed
compress

# RPM packages drop log rotation information into this directory
include /etc/logrotate.d

# no packages own wtmp and btmp -- we'll rotate them here
/var/log/wtmp {
    missingok
    daily
    create 0664 root utmp
    minsize 1M
    rotate 2
}

/var/log/btmp {
    missingok
    daily
    create 0600 root utmp
    rotate 2
}

# system-specific logs may be also be configured here.

EOF

cat > $HOME/logrotate_role/files/logrotate.sh << EOF
## files/logrotate.sh
#!/bin/sh

/usr/sbin/logrotate /etc/logrotate.conf >/dev/null 2>&1
EXITVALUE=$?
if [ $EXITVALUE != 0 ]; then
    /usr/bin/logger -t logrotate "ALERT exited abnormally with [$EXITVALUE]"
fi
exit 0

EOF


cat > $HOME/logrotate_role/files/syslog << EOF
## files/syslog
/var/log/cron
/var/log/maillog
/var/log/messages
/var/log/secure
/var/log/spooler
{
    sharedscripts
    rotate 7
    postrotate
	/bin/kill -HUP `cat /var/run/syslogd.pid 2> /dev/null` 2> /dev/null || true
    endscript
}

EOF

